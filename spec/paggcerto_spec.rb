RSpec.describe Paggcerto do
  before do
    Paggcerto.token = "TOKEN"
  end

  it { expect(Paggcerto.token).to eql("TOKEN") }

  it "has a version number" do
    expect(Paggcerto::VERSION).not_to be nil
  end
end
