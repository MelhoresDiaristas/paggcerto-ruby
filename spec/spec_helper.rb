require 'pry'
require "bundler/setup"
require "paggcerto"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:each) do
    load "./lib/paggcerto.rb"
    #FakeWeb.clean_registry
  end
  
  config.before(:each) do
    Paggcerto.configure do |config|
      config.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IlVzZXIiLCJBcHBsaWNhdGlvbklkIjoiODAiLCJIb2xkZXJJZCI6IjM3MSIsIlVzZXJJZCI6IjM3MSIsIkxpbmtDb2RlIjoiODc1NjI1MTkiLCJTYWx0IjoiYWZkMGUyYWFmZjZlNDNmNjgyMTc4MDQzOTcyMzZhMDMiLCJpc3MiOiJhMDczOGNkZCIsImF1ZCI6ImVhMmI0N2VhYmMwNDMifQ.YbJ--1i22-qvLPud6pPY0CgzwDGxNIj9jOpLR5mGyXU"
    end
  end
end
