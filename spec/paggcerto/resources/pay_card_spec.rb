RSpec.describe Paggcerto::PayCard do
  it "create payment low risk" do
    amount = 100.0
    card = Paggcerto::Card.last
    cards = [Paggcerto::RequestCard.new(card: card, amountPaid: amount)]
    
    billing_address = Paggcerto::BillingAddress.new(name: "Maria dos Santos",
      line1: "Rua das Flores, 81",
      line2: "Condominio Viver Melhor, Ap 001 bloco A",
      zipCode: "49160-000",
      city: "Aracaju",
      state: "SE",
      country: "Brasil")
    custumer = Paggcerto::RequestCustomer.new(name: "Luan Pires",
      email: "contato.luan@email.com",
      taxDocument: "298.150.558-01",
      phone: "(79) 2992-2905",
      mobile: "(79) 99991-0762",
      birthDate: "1994-07-03",
      createdAt: "2019-07-22",
      billingAddress: billing_address,
      shippingAddress: billing_address)

    shopping_carts = [Paggcerto::ResquestShoppingCart.new(
      code: "12345.12345 12345.123456 12345.123456 1 12345678901234",
      sku: "pay#{rand(10000)}",
      category: 201,
      name: "Trajes esportivos.",
      description: "Primeira compra",
      unitCost: 16.05,
      discount: 0,
      quantity: 1,
      createdAt: "2019-07-22 08:51:55")]

    pay = Paggcerto::V3PayCard.new(antifraud: true, riskNotAssumed: "High", amount: amount, cards: cards, sellingKey: "pay#{rand(10000)}", customer: custumer, shoppingCarts: shopping_carts)
    binding.pry
    #expect(Paggcerto::Card.new().site.to_s).to eq(Paggcerto.payment_api_endpoint)
  end

  
  it "create payment Middle risk" do
    amount = 100.55
    card = Paggcerto::Card.last
    cards = [Paggcerto::RequestCard.new(card: card, amountPaid: amount)]
    
    billing_address = Paggcerto::BillingAddress.new(name: "Maria dos Santos",
      line1: "Rua das Flores, 81",
      line2: "Condominio Viver Melhor, Ap 001 bloco A",
      zipCode: "49160-000",
      city: "Aracaju",
      state: "SE",
      country: "Brasil")

    shipping_address = Paggcerto::ShippingAddress.new(name: "Maria dos Santos",
      line1: "Rua das Flores, 81",
      line2: "Condominio Viver Melhor, Ap 001 bloco A",
      zipCode: "49160-000",
      city: "Aracaju",
      state: "SE",
      country: "Brasil")

    custumer = Paggcerto::RequestCustomer.new(name: "Luan Pires",
      email: "contato.luan@email.com",
      taxDocument: "298.150.558-01",
      phone: "(79) 2992-2905",
      mobile: "(79) 99991-0762",
      birthDate: "1994-07-03",
      createdAt: "2019-07-22",
      billingAddress: billing_address,
      shippingAddress: shipping_address)

    shopping_carts = [Paggcerto::ResquestShoppingCart.new(
      code: "12345.12345 12345.123456 12345.123456 1 12345678901234",
      sku: "pay#{rand(10000)}",
      category: 201,
      name: "Trajes esportivos.",
      description: "Primeira compra",
      unitCost: 16.05,
      discount: 0,
      quantity: 1,
      createdAt: "2019-07-22 08:51:55")]

    pay = Paggcerto::V3PayCard.new(antifraud: true, riskNotAssumed: "High", amount: amount, cards: cards, sellingKey: "pay#{rand(10000)}", customer: custumer, shoppingCarts: shopping_carts)
    binding.pry
    #expect(Paggcerto::Card.new().site.to_s).to eq(Paggcerto.payment_api_endpoint)
  end

  it "create payment High risk" do
    amount = 100.88
    card = Paggcerto::Card.last
    cards = [Paggcerto::RequestCard.new(card: card, amountPaid: amount)]
    
    billing_address = Paggcerto::BillingAddress.new(name: "Maria dos Santos",
      line1: "Rua das Flores, 81",
      line2: "Condominio Viver Melhor, Ap 001 bloco A",
      zipCode: "49160-000",
      city: "Aracaju",
      state: "SE",
      country: "Brasil")
    custumer = Paggcerto::RequestCustomer.new(name: "Luan Pires",
      email: "contato.luan@email.com",
      taxDocument: "298.150.558-01",
      phone: "(79) 2992-2905",
      mobile: "(79) 99991-0762",
      birthDate: "1994-07-03",
      createdAt: "2019-07-22",
      billingAddress: billing_address,
      shippingAddress: billing_address)

    shopping_carts = [Paggcerto::ResquestShoppingCart.new(
      code: "12345.12345 12345.123456 12345.123456 1 12345678901234",
      sku: "pay#{rand(10000)}",
      category: 201,
      name: "Trajes esportivos.",
      description: "Primeira compra",
      unitCost: 16.05,
      discount: 0,
      quantity: 1,
      createdAt: "2019-07-22 08:51:55")]

    pay = Paggcerto::V3PayCard.new(antifraud: true, riskNotAssumed: "High", amount: amount, cards: cards, sellingKey: "pay#{rand(10000)}", customer: custumer, shoppingCarts: shopping_carts)
    binding.pry
    #expect(Paggcerto::Card.new().site.to_s).to eq(Paggcerto.payment_api_endpoint)
  end
end
  