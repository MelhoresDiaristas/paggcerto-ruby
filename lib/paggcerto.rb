require 'activeresource'
require_relative 'paggcerto/version'
require_relative 'paggcerto/connection'
require_relative 'paggcerto/errors'
require_relative 'paggcerto/json_formatter_pagg'
require_relative 'paggcerto/basic_object'
require_relative 'paggcerto/resource'
require_relative 'paggcerto/account_resource'
require_relative 'paggcerto/payment_resource'
require_relative 'paggcerto/payment_accounts_resource'
require_relative 'paggcerto/pay'
require_relative 'paggcerto/v3_pay'

Dir[File.expand_path('../paggcerto/resources/*.rb', __FILE__)].map do |path|
  require path
end

module Paggcerto
  class << self
    attr_accessor :open_timeout, :timeout, :application_id, :token, :env
  end

  self.open_timeout = 30
  self.timeout      = 90

  def self.configure(&block)
    yield self
  end

  def self.uris
    @uris ||= production? ? production_uris : sandbox_uris
  end

  def self.production_uris
    { account: "https://account.paggcerto.com.br/api/v2/",
      payments: "https://payments.paggcerto.com.br/api/v2/",
      v3_payments: 'https://payments.paggcerto.com.br/api/v3/',
      payment_accounts: 'https://payment-accounts.paggcerto.com.br/api/v1/' }
      
  end

  def self.sandbox_uris
    { account: 'https://account.sandbox.paggcerto.com.br/api/v2/',
      payments: 'https://payments.sandbox.paggcerto.com.br/api/v2/',
      v3_payments: 'https://payments.sandbox.paggcerto.com.br/api/v3/',
      payment_accounts: 'https://payment-accounts.sandbox.paggcerto.com.br/api/v1/' }
      
  end
  
  def self.account_api_endpoint
    uris[:account]
  end

  def self.payments_api_endpoint
    uris[:payments]
  end

  def self.v3_payments_api_endpoint
    uris[:v3_payments]
  end

  def self.payment_accounts_api_endpoint
    uris[:payment_accounts]
  end

  def self.production?
    env == 'production'
  end

end
