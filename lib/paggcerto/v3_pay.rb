module Paggcerto
  class V3Pay < Pay
     
    def self.set_attr_resource
      self.site = Paggcerto.v3_payments_api_endpoint + "pay"
      self.include_format_in_path = false
      self.format = ::JsonFormatterPagg.new(name.demodulize)
      self.set_token
    end
	end
end



