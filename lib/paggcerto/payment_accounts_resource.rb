module Paggcerto
  class PaymentAccountsResource < Resource
    def self.set_attr_resource
      self.site = Paggcerto.payment_accounts_api_endpoint
      self.include_format_in_path = false
      self.format = ::JsonFormatterPagg.new(name.demodulize)
      self.set_token
    end
	end
end
