
module Paggcerto
  
  class Resource < ActiveResource::Base
    attr_accessor :response_body, :response, :response_code
    after_save :set_response
    def initialize(attributes = {}, persisted = false)
      self.class.set_attr_resource
      super(attributes, persisted)
    end

    def self.destroy 
      set_attr_resource
      super
    end

    def self.all 
      set_attr_resource
      super
    end

    def self.last 
      set_attr_resource
      super
    end

    def self.first 
      set_attr_resource
      super
    end

    def self.find(args={}, args2={})
      set_attr_resource
      super args, args2
    end

    def self.set_token
      self.connection.auth_type = :bearer
      self.connection.bearer_token = Paggcerto.token
    end

    def save
      begin
        super
      rescue => e
        set_response
        raise e
      end
    end
   
    def set_response
      self.response_body = connection.body
      self.response = connection.response
      self.response_code = connection.code
    end

    
    
  end
end
