class JsonFormatterPagg
  include ActiveResource::Formats::JsonFormat

  attr_reader :collection_name

  def initialize(collection_name)
    @collection_name = collection_name.pluralize.downcase
  end

  def decode(json)
    hash = JSON.parse json
    unless hash.is_a? Hash
      JSON.parse hash
    else
      hash[collection_name] || hash
    end
  end

  private

  def remove_root(data)
    if data.is_a?(Hash) && data[collection_name]
      data[collection_name]
    else
      data
    end
  end
end
