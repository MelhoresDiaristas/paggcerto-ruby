module Paggcerto
  class PaymentResource < Resource
     
    def self.cancel(paymentId)
      set_attr_resource
      post("cancel/#{paymentId}")
    end

    def self.set_attr_resource
      self.site = Paggcerto.payments_api_endpoint
      self.include_format_in_path = false
      self.format = ::JsonFormatterPagg.new(name.demodulize)
      self.set_token
    end
	end
end



