class ActiveResource::Connection
  # Creates new Net::HTTP instance for communication with
  # remote service and resources.
  attr_accessor :body, :code, :response
  #for debug
  def http
    http = Net::HTTP.new(@site.host, @site.port)
    http.use_ssl = @site.is_a?(URI::HTTPS)
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE if http.use_ssl?
    http.read_timeout = @timeout if @timeout
    # Here's the addition that allows you to see the output
    http.set_debug_output $stderr
    return http
  end

  def request(method, path, *arguments)
    result = ActiveSupport::Notifications.instrument("request.active_resource") do |payload|
      payload[:method]      = method
      payload[:request_uri] = "#{site.scheme}://#{site.host}:#{site.port}#{path}"
      payload[:result]      = http.send(method, path, *arguments)
    end
    self.body = result.body
    self.code = result.code
    self.response = result.response
    handle_response(result)
  rescue Timeout::Error => e
    raise TimeoutError.new(e.message)
  rescue OpenSSL::SSL::SSLError => e
    raise SSLError.new(e.message)
  end
#
  #def handle_response(response)
  #  binding.pry
  #  case response.code.to_i
  #  when 301, 302, 303, 307
  #    raise(Redirection.new(response))
  #  when 200...400
  #    response
  #  when 400
  #    response
  #    #raise(BadRequest.new(response))
  #  when 401
  #    raise(UnauthorizedAccess.new(response))
  #  when 403
  #    raise(ForbiddenAccess.new(response))
  #  when 404
  #    raise(ResourceNotFound.new(response))
  #  when 405
  #    raise(MethodNotAllowed.new(response))
  #  when 409
  #    raise(ResourceConflict.new(response))
  #  when 410
  #    raise(ResourceGone.new(response))
  #  when 412
  #    raise(PreconditionFailed.new(response))
  #  when 422
  #    raise(ResourceInvalid.new(response))
  #  when 429
  #    raise(TooManyRequests.new(response))
  #  when 401...500
  #    raise(ClientError.new(response))
  #  when 500...600
  #    raise(ServerError.new(response))
  #  else
  #    raise(ConnectionError.new(response, "Unknown response code: #{response.code}"))
  #  end
  #end

end
