module Paggcerto
  class CashoutRequest < PaymentAccountsResource
    self.collection_name = "cashout-request"
    
    def self.transfers_splitter(id)
      set_attr_resource      
      o = OpenStruct.new(get("splitters?splitterId=#{id}"))
      begin
        transfers = []
        o.days.each do |day|
          day["cashoutRequests"].each {|t| transfers << Paggcerto::Transfer.new(t)}
        end
        return transfers
      rescue
        []
      end
    end

    def self.request_transfer_splitter(splitter_id, amount, note)
      set_attr_resource
      Paggcerto::Transfer.new(JSON.parse post("splitters", {}, {splitterId: splitter_id, amount:amount, note: note}.to_json).body)
    end

  end
end
  