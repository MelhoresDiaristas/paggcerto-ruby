#name	
#string Required
#Nome do titular do cartão.
#
# #line1	
#string <= 150 characters Required Este parâmetro não aceita caracteres especiais
#Endereço (rua, avenida, travessa, etc.).
#
# #line2	
#string <= 80 characters
#Complemento do endereço.
#
# #zipCode	
#string <= 9 characters Required Ex: 00000-000
#CEP - Código de Endereçamento Postal.
#
# #city	
#string Required
#Nome da Cidade.
#
# #state	
#string Required
#Sigla do estado.
#
# #country	#string Required #País.
module Paggcerto
  class ShippingAddress < BasicObject
    attr_accessor :name, :line1, :line2, :zipCode, :city, :state, :country

  end
end
