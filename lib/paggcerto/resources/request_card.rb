  #      "id": "a0b1",
  #      "sellingKey": "123",
  #      "amountPaid": 60.44,
  #      "installments": 1,
  #      "credit": true,
  #      "authorization": false,
  #      "daysLimitAuthorization": null},
module Paggcerto
  class RequestCard < BasicObject
    attr_accessor :id, :sellingKey, :amountPaid, :installments, :credit, :authorization, :daysLimitAuthorization, :holderName, :number, :brand, :expirationMonth, :expirationYear, :securityCode

      
    def initialize(attributes = {})
      if attributes[:card]
        set_card_fields(attributes[:card]) 
      else
        @id = attributes.fetch(:id, '')
      end
      @sellingKey = attributes.fetch(:sellingKey, nil)
      @amountPaid = attributes.fetch(:amountPaid, '')
      @daysLimitAuthorization = attributes.fetch(:daysLimitAuthorization, '')
      @installments = attributes.fetch(:installments, 1)
      @credit = attributes.fetch(:credit, true)
      @authorization = attributes.fetch(:authorization, true)
    end
    

    def set_card_fields(card)
      if card.id.blank?
        card.attributes.each do |k,v|
          self.send("#{k}=",v)
        end
      else
        @id = card.id
      end
    end
    
    def card=(card)
      set_card_fields(card)
    end
  end
end
