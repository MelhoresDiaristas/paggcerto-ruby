module Paggcerto
  class SettingSplitter < PaymentAccountsResource
    
    self.collection_name = "splitter"

    def self.find(id)
      self.collection_name = "splitter"
      super id
    end
    
    def self.update_transfer_days(splitters=[])
      set_attr_resource
      self.collection_name = "splitters"
      r = put("transfer-days", {}, {splitters: splitters}.to_json)
    end

    def self.set_attr_resource
      self.site = Paggcerto.payment_accounts_api_endpoint + "settings"
      self.include_format_in_path = false
      self.format = ::JsonFormatterPagg.new(name.demodulize)
      self.set_token
    end
  end
end
