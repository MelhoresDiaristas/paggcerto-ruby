module Paggcerto
  class Bin < PaymentResource

    self.schema = {
      'cardBrand' => :string,
      'regex' => :string,
      'debit' => :boolean,
      'emvSupported' => :boolean,
      'maximumInstallment' => :integer
      }
    
  end
end