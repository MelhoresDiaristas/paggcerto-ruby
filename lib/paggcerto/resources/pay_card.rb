#{
#  "sellingKey": "a0b0",
#  "amount": 160.55,
#  "note": "Venda de trajes esportivos.",
#  "cards": [
#    {
#      "id": "a0b1",
#      "sellingKey": "123",
#      "amountPaid": 60.44,
#      "installments": 1,
#      "credit": true,
#      "authorization": false,
#      "daysLimitAuthorization": null},
#    {}
#  ],
#  "paymentDevice": {
#    "serialNumber": "8000151509001953",
#    "model": "mp5"
#  },
#  "mobileDevice": {
#    "appVersion": "4.0.1",
#    "manufacturer": "Apple",
#    "model": "iPhone 6S Plus"
#  },
#  "geolocation": {
#    "latitude": -10.9525822,
#    "longitude": -37.0582894
#  },
#  "splitters": [{
#    "id": "ab01",
#    "paysFee": true,
#    "salesCommission": 5,
#    "amount": 60.55
#  }]
#}

#sellingKey	
#string <= 50 characters
#Caso deseje associar seu código interno ao pagamento na Pagcerto. Esta chave deve ser única e informada manualmente.
#
# amount	
#number Required Valor monetário
#Valor cobrado. Esse valor não pode ser inferior a R$ 1,00 para venda à vista ou inferior a R$ 5,00 para venda parcelada.
#
# note	
#string <= 255 characters
#Observação para o pagamento.
#
# cards 	
#requestCard Required
#Informações dos cartões utilizados no pagamento. Cada pagamento possui o limite máximo de 3 cartões.
#
# paymentDevice 	
#paymentDevice
#Informações do dispositivo utilizado no pagamento.
#
# mobileDevice 	
#mobileDevice
#Informações do dispositivo móvel (smartphone, tablet, etc.).
#
# geolocation 	
#geolocation
#Geolocalização do usuário no momento da realização do pagamento.
#
# splitters 	
#requestSplitters
#Informações do pagamento com split.
module Paggcerto
  class PayCard < Pay
    self.collection_name = "cards"

    self.schema = {
      'id' => :string,
      'sellingKey' => :string,
      'cards' => :string,
      'amount' => :float
    }

    validates :amount, presence: true
    validates :cards, :presence => true, :length => {:minimum => 1 }
    #validates :sellingKey, :length => {:maximum => 20 }

    def self.capture(paymentId, amount, splitters=[])
      set_attr_resource
      r = put("capture/#{paymentId}", {}, {amount: amount, splitters: splitters}.to_json)
    end

  end
end