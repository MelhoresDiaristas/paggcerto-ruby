#holderName	
#string <= 50 characters Required Este parâmetro aceita somente letras
#Nome do titular da conta bancária.
#
# taxDocument	
#string [ 14 .. 18 ] characters Required CPF: 000.000.000-00 ou CNPJ: 00.000.000/0000-00
#CPF/CNPJ do titular da conta bancária.
#
# bankNumber	
#string <= 20 characters Required Este parâmetro aceita somente números
#Número identificador da instituição financeira. Para encontrar esse número, basta consultar nosso método Consultar Banco.
#
# accountNumber	
#string <= 30 characters Required
#Número da conta bancária.
#
# bankBranchNumber	
#string <= 20 characters Required
#Número da agência.
#
# variation	
#string <= 8 characters Este parâmetro aceita somente números
#Variação da conta. O número de variação existe apenas em algumas contas poupança.
#
# type	
#string <= 8 characters Required
#Tipo de conta: corrente ou poupanca.
#
########
#Exemplo
########
#
#holderName = "Maria dos Santos"
#taxDocument = "123.123.123-87"
#bankNumber = "001"
#accountNumber = "123456-78"
#variation
#bankBranchNumber = "1234-5"
#type = "corrente"

module Paggcerto
  class BankAccount < BasicObject
    attr_accessor :holderName, :taxDocument, :bankNumber, :accountNumber, :bankBranchNumber, :type, :variation
  end
end