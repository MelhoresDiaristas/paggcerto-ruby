  #      "id": "a0b1",
  #      "sellingKey": "123",
  #      "amountPaid": 60.44,
  #      "installments": 1,
  #      "credit": true,
  #      "authorization": false,
  #      "daysLimitAuthorization": null},
  module Paggcerto
    class RequestSplitter < BasicObject
      attr_accessor :id, :paysFee, :salesCommission, :amount
  
      def initialize(attributes = {})
        @id = attributes.fetch(:id, nil)
        @salesCommission = attributes.fetch(:salesCommission, nil)
        @paysFee = attributes.fetch(:paysFee, false)
        @amount = attributes.fetch(:amount, nil)
      end
    end
  end
  