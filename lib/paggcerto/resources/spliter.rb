#
# name	
# string Required
# Nome completo da pessoa ou nome da empresa.
# 
#  transferDays	
# number Required
# Dias para repasse do recebedor do split (2 ou 32 dias).
# 
#  anticipatedTransfer	
# boolean Required
# Informar se o repasse é antecipado ou não.
# 
#  address 	
# address Required
# Endereço comercial.
# 
#  bankAccount 	
# bankAccount Required
# Informações bancárias.
#
#
########
#Exemplo
########
#
# splitter.name = "Produtos Maria LTDA"
# splitter.transferDays = 32
# splitter.anticipatedTransfer = true
# splitter.bankAccount = BankAccount.new
# splitter.address = Address.new
module Paggcerto
  class Splitter < PaymentResource
    
    self.schema = {
      'name' => :string,
      'transferDays' => :integer,
      'anticipatedTransfer' => :boolean,
    }
    
    validates :name, :anticipatedTransfer, :transferDays, presence: true
    validates :transferDays , inclusion: { in: [2,32] }

    def self.find_by_id(id)
      set_attr_resource
      r = get("find/#{id}")
      r["address"] = Paggcerto::Address.new(r["address"])
      r["bankAccount"] = Paggcerto::BankAccount.new(r["bankAccount"])
      new(r, true)
    end

    def transfer_days
      Paggcerto::SettingSplitter.find(self.id)
    end

    def set_transfer_days(monday=true, tuesday=true, wednesday=true, thursday=true, friday=true)
      Paggcerto::SettingSplitter.update_transfer_days([TransferSplitter.new(splitterId: self.id, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday)])
    end

    def build_transfer_days(monday=true, tuesday=true, wednesday=true, thursday=true, friday=true)
      TransferSplitter.new(splitterId: self.id, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday)
    end

    def transfers
      @transfers ||= Paggcerto::CashoutRequest.transfers_splitter(self.id)
    end

    def request_transfer(amount, note)
      @transfer ||= Paggcerto::CashoutRequest.request_transfer_splitter(self.id, amount, note )
    end


    def balance
      @balance ||= Paggcerto::SplitterBalance.get_balance([self.id])
    end
    
  end
end
