module Paggcerto
  class Balance < OpenStruct

    def balanceList
      @balancelist ||= super.map{|b| OpenStruct.new b}
    end

    def balance
      @balance ||= balanceList.first
    end

    def available
      balance.availableBalance
    end

    def current
      balance.currentBalance
    end

    def future
      balance.futureBalance
    end

    def last_update
      balance.lastUpdate.to_time
    end

    def balance_blocked
      balance.balanceBlocked
    end

    def blocked?
      balance_blocked
    end
  end


end
