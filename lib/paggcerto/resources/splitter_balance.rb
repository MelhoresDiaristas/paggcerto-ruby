module Paggcerto
  class SplitterBalance < PaymentAccountsResource
    self.collection_name = "splitter-balance"
    
    def self.get_balance(ids=[])
      Paggcerto::Balance.new( JSON.parse _get_balance(ids) )
    end

    def self._get_balance(ids=[])
      set_attr_resource
      
      post("", {}, {ids: ids}.to_json).body
    end

  end
end