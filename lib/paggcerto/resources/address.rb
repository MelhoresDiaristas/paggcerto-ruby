# cityCode	
# string <= 10 characters Required Este parâmetro aceita somente números
# Código da cidade de acordo com o IBGE.
# 
# district	
# string <= 50 characters Required Este parâmetro não aceita caracteres especiais
# Bairro.
# 
# line1	
# string <= 150 characters Required Este parâmetro não aceita caracteres especiais
# Endereço (rua, avenida, travessa, etc.).
# 
# line2	
# string <= 80 characters
# Complemento do endereço.
# 
# streetNumber	
# string <= 20 characters Required
# Número da residência.
# 
# zipCode	
# string <= 9 characters Required Ex: 00000-000
# CEP - Código de Endereçamento Postal.
 
module Paggcerto
  class Address < BasicObject
    attr_accessor :cityCode, :district, :line1, :line2, :streetNumber, :zipCode, :city, :state

  end
end
