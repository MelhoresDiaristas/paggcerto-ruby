module Paggcerto
  class PayCardCapture < Pay
    self.collection_name = "cards"

    self.schema = {
      'id' => :string,
      'sellingKey' => :string,
      'cards' => :string,
      'amount' => :float
    }

    validates :amount, presence: true
    validates :cards, :presence => true, :length => {:minimum => 1 }
    #validates :sellingKey, :length => {:maximum => 20 }

  
    def self.set_attr_resource
      self.site = Paggcerto.payments_api_endpoint + "pay/cards"
      self.include_format_in_path = false
      self.format = ::JsonFormatterPagg.new(name.demodulize)
      self.set_token
    end

  end
end