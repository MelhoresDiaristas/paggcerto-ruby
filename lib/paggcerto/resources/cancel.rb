module Paggcerto
  class Cancel < PaymentResource
    self.collection_name = "cancel"
    
    def self.cancel_payment(id)
      set_attr_resource
      post(id)
    end
    
	end
end



