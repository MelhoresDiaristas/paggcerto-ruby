module Paggcerto
  class Card < PaymentResource
    
    self.schema = {
      'holderName' => :string,
      'number' => :string,
      'brand' => :string,
      'expirationMonth' => :integer,
      'expirationYear' => :integer,
      'securityCode' => :integer  
    }

    validates :number, :holderName, :securityCode, presence: true
    #validates :expirationMonth, inclusion: { in: [1..12] }
    #validates :expirationYear, :presence => true, :length => {:minimum => 2019 }, :numericality => true

  end
end
