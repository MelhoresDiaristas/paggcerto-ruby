  #      "id": "a0b1",
  #      "sellingKey": "123",
  #      "amountPaid": 60.44,
  #      "installments": 1,
  #      "credit": true,
  #      "authorization": false,
  #      "daysLimitAuthorization": null},
  module Paggcerto
    class TransferSplitter < BasicObject
      attr_accessor :splitterId, :monday, :tuesday, :wednesday, :thursday, :friday
      
      def initialize(attributes = {})
        @splitterId = attributes.fetch(:splitterId, nil)
        @monday = attributes.fetch(:monday, false)
        @tuesday = attributes.fetch(:tuesday, false)
        @wednesday = attributes.fetch(:wednesday, false)
        @thursday = attributes.fetch(:thursday, false)
        @friday = attributes.fetch(:friday, false)
      end
    end
  end
  