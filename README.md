# paggcerto-ruby


Paggcerto Ruby library 

## Documentation

* [Documentation](https://desenvolvedor.paggcerto.com.br/v2/account/)

## Getting Started

### Install

```shell
gem install paggcerto
```
or add the following line to Gemfile:

```ruby
gem 'paggcerto'
```
and run `bundle install` from your shell.

### Configure your API key

You can set your API key in Ruby:

```ruby
Paggcerto.api_key        = 'YOUR_API_KEY_HERE'
Paggcerto.encryption_key = 'YOUR_ENCRYPTION_KEY_HERE' # If needed
```

### TODO

## Support
If you have any problem or suggestion please open an issue [here](https://github.com).

## License

Check [here](LICENSE).
